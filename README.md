# Image Gallery
Image Gallery is a web application that let users register, create albums and add images to them.

## Description
After login (or registration) the user can see his albums or create a new one. If the user selects an album, all the images inside the album are showed. By clicking on an image the user can see all its details and comments on it. He can also add a new comment. Finally, the user can add a new image to the album.