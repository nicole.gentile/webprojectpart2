package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import beans.User;
import connectionHandler.ConnectionHandler;
import dao.AlbumDAO;

/**
 * Servlet implementation class SaveAlbumsOrder
 */
@WebServlet("/SaveAlbumsOrder")
@MultipartConfig
public class SaveAlbumsOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection;

	public void init() throws ServletException {
		connection = ConnectionHandler.getConnection(getServletContext());
	}   
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveAlbumsOrder() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String res = request.getParameter("positions");//list of ids
		User user = (User) request.getSession().getAttribute("user");

		//check that the list of ids is not null
		if( res == null || res.split(",").length == 0) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error");
			return;
		}
		int idAlbum = 0;
		String[] positions = res.split(",");
		List<Integer> albumPositions = new ArrayList<Integer>();
		AlbumDAO aDAO  = new AlbumDAO(connection);

		try {
			//Cast to array of integer
			for(int i = 0; i < positions.length; ++i) {
				idAlbum = Integer.parseInt(positions[i]);
				if(albumPositions.size() > 0 && albumPositions.contains(idAlbum)) {
					//two albums with the same id
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					response.getWriter().println("New albums order can not be saved due to request error");
					return;
				} else if(aDAO.findOwner(idAlbum) != user.getId()){
					//the user is not the owner of the current album
					response.setStatus(HttpServletResponse.SC_FORBIDDEN);
					response.getWriter().println("You are not authorized to change albums order");
					return;
				} else {
					albumPositions.add(idAlbum);
				}
			}
		} catch (NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error");
			return;
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Error while executing query");
			return;
		} 
		
		//try to update albums order on the database
		try {
			aDAO.updateAlbumsOrder(albumPositions);
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("New albums order can not be saved due to server error");
			return;
		}
		
		response.setStatus(HttpServletResponse.SC_OK);
	}

	public void destroy() {
		try {
			ConnectionHandler.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
