package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import beans.Comment;
import beans.Image;
import beans.User;
import connectionHandler.ConnectionHandler;
import dao.AlbumDAO;
import dao.CommentDAO;
import dao.ImageDAO;

/**
 * Servlet implementation class GetImageDetails
 */
@WebServlet("/GetImageDetails")
@MultipartConfig
public class GetImageDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection;

	public void init() throws ServletException {
		connection = ConnectionHandler.getConnection(getServletContext());
	}  
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetImageDetails() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idImageStr = request.getParameter("idimage");
		String idAlbumStr = request.getParameter("idalbum");
		
		//check that image id and album id are not null	
		if(idImageStr == null || idImageStr == "" || idAlbumStr == null || idAlbumStr == "") {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error, please retry");
			return;
		}
		int idImage = 0;
		int idAlbum = 0;
		try {
			idImage = Integer.parseInt(idImageStr);
			idAlbum = Integer.parseInt(idAlbumStr);
		} catch(NumberFormatException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error, please retry");
			return;
		}
		
		User user = (User)(request.getSession().getAttribute("user"));
		AlbumDAO aDAO = new AlbumDAO(connection);
		ImageDAO iDAO = new ImageDAO(connection, idAlbum);
		CommentDAO cDAO = new CommentDAO(connection);
		Image image = null;
		List<Comment> comments = null;
		try {
			//the user is not the album's owner 
			if(aDAO.findOwner(idAlbum) != user.getId()) {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("You are not authorized to open this album");
				return;
			}
			//getImageDetails() checks that the image belongs to the current album
			image = iDAO.getImageDetails(idImage);
			if(image == null) {
				//no image with idImage belongs to the current album
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("You do not have the permission to open this image");
				return;
			}
			comments = cDAO.getComments(idImage);
			image.setComments(comments);
			String json = new Gson().toJson(image);
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Internal server error, retry later");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	public void destroy() {
		try {
			ConnectionHandler.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
