package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import connectionHandler.ConnectionHandler;
import dao.AlbumDAO;
import dao.CommentDAO;
import dao.ImageDAO;

/**
 * Servlet implementation class AddComment
 */
@WebServlet("/AddComment")
@MultipartConfig
public class AddComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection;

	public void init() throws ServletException {
		connection = ConnectionHandler.getConnection(getServletContext());
	} 
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddComment() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String comment = request.getParameter("text");
		String idImageStr = request.getParameter("idimage");
		String idAlbumStr = request.getParameter("idalbum");
		User user = (User)request.getSession().getAttribute("user");
				
		//check comment is not null
		if(comment == null || comment.equals("") ) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Can not insert null comment");
			return;
		}
		//check that image id and album id are correct		
		if(idImageStr == null || idImageStr.equals("")  || idAlbumStr == null || idAlbumStr.equals("") ) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error, please retry");
			return;
		}
		int idImage = 0, idAlbum = 0;
		try {
			idAlbum = Integer.parseInt(idAlbumStr);
			idImage = Integer.parseInt(idImageStr);
		} catch(NumberFormatException e ) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error, please retry");
			return;
		}
		
		AlbumDAO aDAO = new AlbumDAO(connection);
		ImageDAO iDAO = new ImageDAO(connection, idAlbum);
		CommentDAO cDAO = new CommentDAO(connection);
		
		try {
			//the user is not the album's owner or the image does not belong to the selected album
			if(aDAO.findOwner(idAlbum) != user.getId() || !iDAO.belongsToAlbum(idImage)) {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("You are not authorized");
				return;
			}
			
			cDAO.createComment(idImage, comment, user.getUsername());
			
		} catch(SQLException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Internal server error, retry later");
			return;			
		}
		
		response.setStatus(HttpServletResponse.SC_OK);
	}

	public void destroy() {
		try {
			ConnectionHandler.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
