package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import beans.User;
import connectionHandler.ConnectionHandler;
import dao.UserDAO;

/**
 * Servlet implementation class RegisterUser
 */
@WebServlet("/RegisterUser")
@MultipartConfig
public class RegisterUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection;

	public void init() throws ServletException {
		connection = ConnectionHandler.getConnection(getServletContext());
	}   
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String repeatPassword =  request.getParameter("repeatPassword");
		String email = request.getParameter("email");

		//check email validity
		Pattern regex = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
		if (!regex.matcher(email).matches()) {
			//email address is not valid
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Please input a valid email address");
			return;
		}
		
		//check username, password and repeat password are not null
		if(username == null || username.equals("") ||
				password == null || password.equals("") ||
				email == null || email.equals("") ||
				repeatPassword == null || repeatPassword.equals("")) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Please fill all fields of the form");
			return;
		}
		
		//check that password is equal to repeat password
		if(!password.equals(repeatPassword)) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Password and repeat password do not match");
			return;
		}

		UserDAO uDAO = new UserDAO(connection);
		User user = null;
		try {
			//check if the chosen username is available
			if(uDAO.existingUsername(username)) { //not available
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("Choose another username");
				return;
			}
			//username is available: register the user
			uDAO.registerUser(username, password, email);
			user = uDAO.checkCredentials(username, password); //get user id
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Internal server error, retry later");
			return;
		}
		request.getSession().setAttribute("user", user);
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().println(username);
	}

	public void destroy() {
		try {
			ConnectionHandler.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
