package controllers;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.User;
import connectionHandler.ConnectionHandler;
import dao.AlbumDAO;

@WebServlet("/CreateAlbum")
public class CreateAlbum extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection;  

	public void init() throws ServletException {
		connection = ConnectionHandler.getConnection(getServletContext());    	
	}    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateAlbum() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("title");
		
		//check that the title is not null
		if(title == null || title.equals("")) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Can not insert null title");
			return;
		}
		int idUser = ((User) request.getSession().getAttribute("user")).getId();
		AlbumDAO aDAO = new AlbumDAO(connection);
		//create new album
		try {
			aDAO.createNewAlbum(idUser, title);
		} catch (SQLException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Error while executing query, please retry");
			return;
		}
		
		//Create folder in tomcat 
		String path = System.getProperty("catalina.base")+"/webapps/images/";
		File saveDir = new File(path + title + "/");
		if(!saveDir.exists()) {
			saveDir.mkdirs();
		}	
		response.setStatus(HttpServletResponse.SC_OK);
	}
	
	public void destroy() {
		try {
			ConnectionHandler.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
