package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import beans.Image;
import beans.User;
import connectionHandler.ConnectionHandler;
import dao.AlbumDAO;
import dao.ImageDAO;

/**
 * Servlet implementation class GetImages
 */
@WebServlet("/GetImages")
@MultipartConfig
public class GetImages extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Connection connection;
    
	public void init() throws ServletException {
		connection = ConnectionHandler.getConnection(getServletContext());
	}     
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetImages() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idAlbum = 0;
		User user = (User) request.getSession().getAttribute("user");
		String idAlbumStr = request.getParameter("idalbum");
		
		//check album id is not null
		if(idAlbumStr == null || idAlbumStr.equals("") ) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error: no album id");
			return;
		}
		try {
			idAlbum = Integer.parseInt(idAlbumStr);
		} catch (NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error: wrong album id");
			return;
		}
		
		AlbumDAO aDAO = new AlbumDAO(connection);
		ImageDAO iDAO = new ImageDAO(connection, idAlbum);
		try {
			//the user is not the album's owner
			if(aDAO.findOwner(idAlbum) != user.getId()) {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("You are not allowed to open this album");
				return;
			}
			//get album images
			List<Image> images = iDAO.getImages();
			String json = new Gson().toJson(images);
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
			
		} catch (SQLException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Internal server error, retry later");
			return;
		}
	}

	public void destroy() {
		try {
			ConnectionHandler.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
