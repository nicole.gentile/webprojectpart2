package controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.lang.RandomStringUtils;

import beans.User;
import connectionHandler.ConnectionHandler;
import dao.AlbumDAO;
import dao.ImageDAO;

/**
 * Servlet implementation class UploadImages
 */

@MultipartConfig
@WebServlet("/UploadImage")
public class UploadImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection;  

	public void init() throws ServletException {
		connection = ConnectionHandler.getConnection(getServletContext());    	
	}
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UploadImage() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tomcatBase = System.getProperty("catalina.base");
		String path1 = "/images/";
		String path = "/webapps" + path1;		
		Part part = null;
		String idAlbumStr = request.getParameter("idalbum");
		String title = request.getParameter("title");//image title
		String text = request.getParameter("text");//image description
		String albumTitle = request.getParameter("albumtitle");
		
		//check that album id and album title are not null 
		if(idAlbumStr == null || idAlbumStr.equals("")  || albumTitle == null || albumTitle.equals("") ) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error: no album selected");
			return;
		}
	
		//check that image title and image description are not null 
		if(title == null || title.equals("") || text == null || text.equals("") ) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Request error: please fill all fields of the form");
			return;
		}
		
		int idAlbum = 0;
		try {
			idAlbum = Integer.parseInt(idAlbumStr);
		} catch(NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Error: wrong album id");
			return;
		}
		
		AlbumDAO aDAO = new AlbumDAO(connection);
		HttpSession session = request.getSession();
		try {
			if(((User)session.getAttribute("user")).getId() != aDAO.findOwner(idAlbum)) {
				//the user is not the owner of the current album
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("Cannot upload image in the selected album");
				return;
			}
		} catch (SQLException | IOException e1) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Server error, please retry later");
			return;
		}	

		//check that the file has been uploaded correctly  
		try {
			part = request.getPart("file");
			if(part == null || part.getSize() == 0) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("Cannot upload an empty file");
				return;
			}
		}
		catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Error during the upload, please retry");
			return;
		}
		
		String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
		String extension = filename.substring(filename.indexOf(".")); 
		//check that the file extension is correct (the file must be an image)
		if(!extension.equals(".jpg") &&
				!extension.equals(".png") && 
				!extension.equals(".jpeg") && 
				!extension.equals(".gif")) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid file extension. The file to upload must have one of the following extension: jpg, jpeg, png, gif");
			return;	
		}
		
		//Create folder if it does not exist yet
		File saveDir = new File(tomcatBase + path + albumTitle + "/");
		if(!saveDir.exists()) {
			saveDir.mkdirs();
		}

		//Assing a random name to the file
	    String codedFilename = RandomStringUtils.randomAlphanumeric(20) + extension;
	    
	    //Save file on server
	    part.write(tomcatBase + path + albumTitle + "/" + codedFilename);

		//Save the image into the database
		ImageDAO iDAO = new ImageDAO(connection, idAlbum);
		try {
			iDAO.saveImage(title, text, path1 + albumTitle + "/" + codedFilename);
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Server error, please retry later");
			return;
		}
		response.setStatus(HttpServletResponse.SC_OK);
	}

	public void destroy() {
		try {
			ConnectionHandler.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
