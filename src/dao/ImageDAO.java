package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Image;

public class ImageDAO {
	private Connection connection;
	private int idAlbum;

	public ImageDAO(Connection connection,int idAlbum) {
		this.connection = connection;
		this.idAlbum = idAlbum;
	}

	
	/**
	 * Gets all the images of the album which id is idAlbum 
	 * @return a list of Image elements
	 * @throws SQLException
	 */
	public List<Image> getImages() throws SQLException{
		List<Image> images = new ArrayList<Image>();
		String query = "SELECT idimage,path,title FROM image WHERE idalbum=? ORDER BY date DESC, idimage DESC";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setInt(1, idAlbum);
			rs = ps.executeQuery();
			while(rs.next()) {
				Image image = new Image();
				image.setIdImage(rs.getInt("idimage"));
				image.setPath(rs.getString("path"));
				image.setTitle(rs.getString("title"));
				images.add(image);
			}
		} catch (SQLException e) {
			throw new SQLException(e);

		} finally {
			try {
				rs.close();
			} catch (Exception e1) {
				throw new SQLException(e1);
			}
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
		return images;
	}

	/**
	 * Gets all the details(date, title, text and path) of an image
	 * @param idImage - id of the image
	 * @return an Image object whose attributes specifies the details to show
	 * @throws SQLException
	 */
	public Image getImageDetails(int idImage) throws SQLException{
		Image image = null;
		String query = "SELECT date,text,title,path FROM image WHERE idimage=? AND idalbum=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setInt(1, idImage);
			ps.setInt(2, idAlbum);
			rs = ps.executeQuery();
			while(rs.next()) {
				image = new Image();
				image.setDate(rs.getDate("date"));
				image.setIdImage(idImage);
				image.setIdAlbum(idAlbum);
				image.setText(rs.getString("text"));
				image.setPath(rs.getString("path"));
				image.setTitle(rs.getString("title"));
			}
		} catch (SQLException e) {
			throw new SQLException(e);

		} finally {
			try {
				rs.close();
			} catch (Exception e1) {
				throw new SQLException(e1);
			}
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
		return image;
	}
	
	/**
	 * Save a new image on the database
	 * @param title - title of the image
	 * @param text - description of the image
	 * @param path - path of the image on server
	 * @throws SQLException
	 */
	public void saveImage(String title, String text, String path) throws SQLException{
		String query = "INSERT INTO image(title,text,path,idalbum) VALUES(?,?,?,?)";
		PreparedStatement ps = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setString(1, title);
			ps.setString(2, text);
			ps.setString(3, path);
			ps.setInt(4, idAlbum);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLException(e);
		} finally {
			try {
				ps.close();
			} catch (Exception e1) {

			}
		}
	}
	
	/**
	 * Checks if the image with id = idImage belongs to the 
	 * current album
	 * @param idImage
	 * @return true if the image belongs to the album, false otherwise
	 * @throws SQLException
	 */
	public boolean belongsToAlbum(int idImage) throws SQLException{
		List<Image> images = new ArrayList<Image>();
		boolean belonging = true;
		String query = "SELECT idimage FROM image WHERE idalbum=? AND idimage=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setInt(1, idAlbum);
			ps.setInt(2, idImage);
			rs = ps.executeQuery();
			if(!rs.isBeforeFirst()) {
				belonging = false;
			}
		} catch (SQLException e) {
			throw new SQLException(e);

		} finally {
			try {
				rs.close();
			} catch (Exception e1) {
				throw new SQLException(e1);
			}
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
		return belonging;
	}

}
