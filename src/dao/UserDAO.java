package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.User;

public class UserDAO {
	private Connection connection;
 
	public UserDAO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Check credentials of user in order to log in
	 * @param username
	 * @param password
	 * @return a User object if credentials are correct, null otherwise
	 * @throws SQLException
	 */
	public User checkCredentials(String username, String password) throws SQLException {
		User user = null;
		String query = "SELECT * FROM user WHERE username=? AND password=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setString(1, username);
			ps.setString(2, password);
			rs = ps.executeQuery();
			if(rs.next()) {
				user = new User();
				user.setId(rs.getInt("iduser"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException(e);
		}
		return user;
	}

	/**
	 * Checks if the username chosen by the user is available or not
	 * @param username - the chosen username 
	 * @return true if the username is not available, false otherwise
	 * @throws SQLException
	 */
	public boolean existingUsername(String username) throws SQLException {
		boolean existing = false;
		String query = "SELECT username FROM user WHERE username = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, username);
			rs = ps.executeQuery();
			if(rs.next()) {
				existing = true;
			}
		} catch (SQLException e) {
			throw new SQLException(e);
		}
		return existing;
	}

	/**
	 * Saves a new user into the database
	 * @param username
	 * @param password
	 * @throws SQLException
	 */
	public void registerUser(String username, String password, String email) throws SQLException{
		String query = "INSERT INTO user(username, password, email) VALUES (?,?,?)";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, email);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLException(e);
		} finally {
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
	}
}
