package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import beans.Album;

public class AlbumDAO {
	private Connection connection;

	public AlbumDAO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Finds all the albums owned by a user
	 * @param idUser id of the user
	 * @return List of the albums of the user
	 * @throws SQLException
	 */
	public List<Album> getAlbums(int idUser) throws SQLException{
		List<Album> albums = new ArrayList<Album>();
		String query = "SELECT * FROM album WHERE owner=? ORDER BY position ASC, date DESC";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setInt(1, idUser);
			rs = ps.executeQuery();
			while(rs.next()) {
				Album album = new Album();
				album.setDate(rs.getDate("date"));
				album.setIdAlbum(rs.getInt("idAlbum"));
				album.setTitle(rs.getString("title"));
				albums.add(album);
			}
		} catch (SQLException e) {
			throw new SQLException(e);

		} finally {
			try {
				rs.close();
			} catch (Exception e1) {
				throw new SQLException(e1);
			}
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}		
		return albums;
	}

	/**
	 * Finds the owner of an album
	 * @param idAlbum - id of the album
	 * @return  id of the owner
	 * @throws SQLException
	 */
	public int findOwner(int idAlbum) throws SQLException {
		int owner = 0;
		String query = "SELECT owner FROM album WHERE idalbum=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setInt(1, idAlbum);
			rs = ps.executeQuery();
			if(rs.next()) {
				owner = rs.getInt("owner");
			}
		} catch (SQLException e) {
			throw new SQLException(e);

		} finally {
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
		return owner;
	}
	
	/**
	 * Create a new album
	 * @param owner - username of tha album's owner
	 * @param title - title of the new album
	 * @throws SQLException
	 */
	public void createNewAlbum(int owner, String title) throws SQLException {
		String query = "INSERT INTO album(title,owner,position) VALUES(?,?,?)";
		PreparedStatement ps = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setString(1, title);
			ps.setInt(2, owner);
			ps.setInt(3, 0);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLException(e);
		} finally {
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
	}

	/**
	 * Updates albums'order into the database using an SQL transaction
	 * @param albumIds - List of albums' ids representing the new albums' order
	 * @throws SQLException
	 */
	public void updateAlbumsOrder(List<Integer> albumIds) throws SQLException {
		String query = "UPDATE album SET position=? WHERE idalbum=?";
		PreparedStatement ps = null;

		try {
			connection.setAutoCommit(false);
			ps = connection.prepareStatement(query);

			for (int i = 0; i < albumIds.size(); ++i) {
				ps.setInt(1, i + 1);
				ps.setInt(2, albumIds.get(i));
				ps.executeUpdate();
			}
			connection.commit();
			
		} catch (SQLException e ) {
			if (connection != null) {
				try {
					connection.rollback();
				} catch(SQLException excep) {
					excep.printStackTrace();
				}
				throw new SQLException(e);
			}
		} finally {
	        if (ps != null) {
	            ps.close();
	        }
	        connection.setAutoCommit(true);
	    }
	}
}
