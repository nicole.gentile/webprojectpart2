package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;

public class CommentDAO {
	private Connection connection;

	public CommentDAO(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * Finds comments on an image
	 * @param idImage
	 * @return List of comments on the selected image
	 * @throws SQLException
	 */
	public List<Comment> getComments(int idImage) throws SQLException{
		List<Comment> comments = new ArrayList<Comment>();
		String query  = "SELECT * FROM comment WHERE idimage=? ORDER BY date DESC";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setInt(1, idImage);
			rs = ps.executeQuery();
			while(rs.next()) {
				Comment comment = new Comment();
				comment.setAuthor(rs.getString("author"));
				comment.setDate(rs.getDate("date"));
				comment.setIdComment(rs.getInt("idcomment"));
				comment.setIdImage(idImage);
				comment.setText(rs.getString("text"));
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new SQLException(e);

		} finally {
			try {
				rs.close();
			} catch (Exception e1) {
				throw new SQLException(e1);
			}
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
		return comments;
	}

	/**
	 * Creates a comment 
	 * @param idImage - id of the image the user wants to comment
	 * @param text - text of the comment
	 * @param author - username of the author
	 * @throws SQLException
	 */
	public void createComment(int idImage, String text, String author) throws SQLException {
		String query  = "INSERT INTO comment(idimage,text,author) VALUES(?,?,?)";
		PreparedStatement ps = null;
		try{
			ps = connection.prepareStatement(query);
			ps.setInt(1, idImage);
			ps.setString(2, text);
			ps.setString(3, author);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLException(e);
		} finally {
			try {
				ps.close();
			} catch (Exception e2) {
				throw new SQLException(e2);
			}
		}
	}
}
