function makeCall(method, url, formElement, callBack, reset = true){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function(){
        callBack(x);
    };
    x.open(method, url);
    if(formElement == null){
        x.send();
    } else {
        x.send(new FormData(formElement));
    }
    if (formElement !== null && reset === true) {
	      formElement.reset();
	    }
}

function makeCallJson(method, url, bodyReq, callBack, reset = true){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function(){
        callBack(x);
    };
    x.open(method, url+bodyReq);
    x.send();
}