(function() {

	var alertmessage = document.getElementById("alertmessage"),
	albumsList, 
	albumImages = new AlbumImages(document.getElementById("albumpagecontainer"), 0, ""), 
	imageDisplayed, personalMessage,
	saveOrderButton = document.getElementById("id_saveorderbutton"),
	addAlbumButton = document.getElementById("id_addalbumbutton"),
	addImageButton = document.getElementById("id_addimagebutton"),
	addImageButtonDiv = document.getElementById("id_addimagebuttondiv"),
	backToHomePageButton = document.getElementById("id_backtohomepage"),
	logoutButton = document.getElementById("id_logoutbutton"),
	commentForm = document.getElementById("id_addcommentform"),
	addAlbumForm = document.getElementById("id_addalbumform"),
	addImageForm = document.getElementById("id_addimageform"),
	pageOrchestrator = new PageOrchestrator();


	//initialize page components
	window.addEventListener("load", () => {
		pageOrchestrator.start();
	}, false);


	function PersonalMessage(_username, messagecontainer) {
		this.username = _username;

		this.show = function() {
			messagecontainer.textContent = this.username;
		}
	}

	function AlbumsList(_albumcontainer){
		this.albumcontainer = _albumcontainer;

		//hide albums list
		this.reset = function() {
			this.albumcontainer.style.display = "none";
		};

		//show albums list
		this.refresh = function(){
			this.albumcontainer.style.display = "block";
		};

		//Get all user's albums
		this.show = function(){
			var self = this;
			makeCall("GET", "GetAlbums", null, function(req){
				if(req.readyState == 4){
					var res = req.responseText;
					if(req.status == 200){
						self.update(JSON.parse(res));
					} else {
						alertmessage.textContent = res;
					}
				}
			});  
		};

		//update the view to show user's albums
		this.update = function(albums){
			var l = albums.length, listItem, album, anchor,
			albumsList = document.getElementById("id_albumlist");
			if(l == 0){
				alertmessage.textContent = "No albums to display"; 
			} else {
				var i;           	
				alertmessage.innerHTML = "";
				for(i = 0; i < l; ++i){
					listItem = document.createElement("li");
					anchor = document.createElement("a");
					listItem.appendChild(anchor);
					anchor.setAttribute("idalbum",albums[i].idAlbum);
					anchor.setAttribute("albumtitle",albums[i].title);
					anchor.addEventListener('click', (e)=>{
						pageOrchestrator.openAlbum(e.target.getAttribute("idalbum"), e.target.getAttribute("albumtitle")); 
					});
					anchor.appendChild(document.createTextNode(albums[i].title + "       " + albums[i].date));
					anchor.href = "#";
					albumsList.appendChild(listItem);
				} 
			}
		};

		//Update albums order
		this.updateorder = function(){
			var items = Array.from(document.querySelectorAll('ul>li>a')),
			positions = "";
			for(var i = 0; i < items.length; ++i){
				positions += items[i].getAttribute("idalbum");
				if(i != items.length - 1){
					positions +=  ",";
				}
			}
			makeCallJson("POST", 'SaveAlbumsOrder?positions=', positions, function(req){
				if(req.readyState == 4){
					var res = req.responseText;
					if(req.status == 200){
						saveOrderButton.style.visibility = "hidden";
					} else {
						alertmessage.textContent = res;
					}
				}
			});
		};
	}

	function AlbumImages(_albumpagecontainer, _idalbum, _title){
		this.imagelist = [];
		this.albumpage = _albumpagecontainer;
		this.idalbum = _idalbum;
		this.title = _title;
		this.currentPosition = 0;

		//hide images
		this.reset = function(){
			this.albumpage.style.display = "none";
		};

		//get images and show them
		this.show = function(idalbum){
			var self = this;

			makeCall("GET",'GetImages?idalbum=' + idalbum, null, function(req){
				if(req.readyState == 4){
					var res = req.responseText;
					if(req.status == 200){
						self.imagelist = JSON.parse(res);
						self.update(0);
						self.albumpage.style.display  = "block";
					} else {
						alertmessage.textContent = res;
					}
				}
			});
		};
		
		//load the thumbnails
		this.update = function(position){
			var i, s = 1, elem, p;
			var self = this;

			document.getElementById("id_albumtitle").textContent = this.title;
			this.currentPosition = this.currentPosition + position;

			if(this.imagelist.length === 0){
				alertmessage.textContent = "No images to display";
			} else {
				alertmessage.innerHTML = "";
			}

			//show or hide prev and next button
			if(this.currentPosition > 0){
				document.getElementById("id_prev").style.visibility = "visible";
			} else {
				document.getElementById("id_prev").style.visibility = "hidden";
			}
			if(this.currentPosition + 5 < self.imagelist.length){
				document.getElementById("id_next").style.visibility = "visible";
			} else {
				document.getElementById("id_next").style.visibility = "hidden";
			}

			//update images to show
			for(i = this.currentPosition; i < this.currentPosition + 5; ++i){
				elem = document.getElementById("img" + s);
				document.querySelectorAll('.image ')[s - 1].style.visibility = "visible";
				elem.style.visibility = "visible"; 
				p = document.querySelectorAll('.image p')[s - 1];

				if(i + 1 <= self.imagelist.length){
					elem.setAttribute("src", self.imagelist[i].path);
					elem.setAttribute("idimage", self.imagelist[i].idImage);                   
					elem.setAttribute("idalbum", self.idalbum);   
					p.textContent = self.imagelist[i].title;
				} else {
					//less than five images
					elem.setAttribute("src", "");
					elem.style.visibility = "hidden";
					document.querySelectorAll('.image ')[s - 1].style.visibility = "hidden";
					p.textContent = "";
				}
				++s;
			}
			if(self.imagelist.length == 0){
				document.getElementById("row").style.display = "none";
			} else {
				document.getElementById("row").style.display = "block";
			}
			addImageButtonDiv.style.display = "block";
			addImageForm.style.display = "none";
		};
	}

	function Image(_idimage, _idalbum){
		var img;
		this.idimage = _idimage;
		this.idalbum = _idalbum;
		this.commentsContainer = document.getElementById("id_commentcontainer");
		this.commentForm = document.getElementById("id_addcommentform");
		this.imageDetailsContainer = document.getElementById("id_imagedetailscontainer");
		this.modal = document.getElementById("id_modal");

		//get all the details of the image
		this.show = function(){
			var self = this, imageToDisplay;
			makeCall("GET", 'GetImageDetails?idimage='+self.idimage+'&idalbum='+self.idalbum,null, function(req){
				if(req.readyState == 4){
					var res = req.responseText;
					if(req.status == 200){
						imageToDisplay = JSON.parse(res);
						img = imageToDisplay;
						self.update(imageToDisplay);
					} else {
						alertmessage.textContent = res;
					}
				}
			});
		};
		//open modal image
		this.update = function(imageToDisplay){
			var comments = [], i, p, 
			pDate = this.imageDetailsContainer.querySelectorAll("p")[0], 
			pText = this.imageDetailsContainer.querySelectorAll("p")[1], 
			hTitle = this.imageDetailsContainer.querySelector("h1");
			comments = imageToDisplay.comments; 

			//show details
			hTitle.textContent = imageToDisplay.title;
			pDate.textContent = imageToDisplay.date;
			pText.textContent = imageToDisplay.text;

			//reset comments container
			while(this.commentsContainer.hasChildNodes()){
				this.commentsContainer.removeChild(this.commentsContainer.querySelectorAll("p")[0]);
			}
			if(comments.length > 0){
				this.commentsContainer.style.display = "block";
			} else {
				this.commentsContainer.style.display = "none";
			}
			//show comments
			for(i = 0; i < comments.length; ++i){
				p = document.createElement("p");
				p.appendChild(document.createTextNode(comments[i].author + ": " +comments[i].text));
				this.commentsContainer.appendChild(p);
			}
			this.commentForm.elements["idimage"].value = imageToDisplay.idImage;
			this.commentForm.elements["idalbum"].value = this.idalbum;
			this.modal.style.display = "block";
			document.getElementById("id_imagetodisplay").setAttribute("src", imageToDisplay.path); 
			document.getElementById("id_addcommentalertmessage").innerHTML = "";
		};

		//hide modal window
		this.close = function(){
			this.modal.style.display = "none";
		};
	}

	function PageOrchestrator(){
		
		//initialize view components
		this.start = function(){
			personalMessage = new PersonalMessage(sessionStorage.getItem('username'), document.getElementById("id_username"));
			personalMessage.show();
			albumsList = new AlbumsList(document.getElementById("albumlistcontainer"));
			albumsList.show();
			this.inizializeListeners();
		};

		//inizialize listeners
		this.inizializeListeners = function(){

			saveOrderButton.addEventListener('click', function(){
				albumsList.updateorder();
			});

			backToHomePageButton.addEventListener('click', function(){
				pageOrchestrator.backToHomePage();
			});

			addAlbumButton.addEventListener('click', function(){
				addAlbumForm.style.display = "block";
				addAlbumButton.style.display = "none";
			});

			addImageButton.addEventListener('click', function(){
				document.getElementById("addimageformcontainer").style.display = "block";
				addImageButtonDiv.style.display = "none";
				addImageForm.style.display = "block";
				addImageForm.elements["idalbum"].value = albumImages.idalbum;
				addImageForm.elements["albumtitle"].value = albumImages.title;
			});

			//submit form to create a new album
			addAlbumForm.elements["submit"].addEventListener('click', function(){
				var form = document.getElementById("id_addalbumform");
				if(form.elements["title"].value == ""){
					alertmessage.textContent = "Cannot create album with empty title";
				} else {
					var self = this;		
					if(form.checkValidity()){
						makeCall("POST", 'CreateAlbum?title='+form.elements["title"].value, null, function(req){
							if(req.readyState == 4){
								var res = req.responseText;
								if(req.status == 200){
									//simulate user interaction
									var e = document.createEvent("HTMLEvents");
									e.initEvent("change", false, true);
									document.getElementById("id_albumlist").dispatchEvent(e);
									form.reset();

								} else {
									alertmessage.textContent = res;
								}
							}
						});
					} else {
						form.reportValidity();
					}
				}
			});

			//simulate user interaction to show all albums
			document.getElementById("id_albumlist").addEventListener('change',function(){
				document.getElementById("id_albumlist").innerHTML = "";
				addAlbumButton.style.display = "block";
				addAlbumForm.style.display = "none";
				albumsList.show();
			}); 

			logoutButton.addEventListener('click', function(){
				window.sessionStorage.removeItem('username');
			});       

			//close modal image
			var span = document.getElementsByClassName("close")[0];
			span.addEventListener('click', function() {
				imageDisplayed.close();
			});

			//submit form to add a new image to an album
			addImageForm.elements["submit"].addEventListener('click', function(){
				var form = document.getElementById("id_addimageform");
				var file = form.elements["file"];
				
				if(form.elements["title"].value == "" || form.elements["text"].value == ""){
					alertmessage.textContent = "Please fill all fields of the form";
				} else if (file.value == ""){
					alertmessage.textContent = "No image selected";
				} else if(file.value.split('.')[1] != "jpg" && 
						file.value.split('.')[1] != "jpeg" && 
						file.value.split('.')[1] != "png" && 
						file.value.split('.')[1] != "gif"){
					alertmessage.textContent = "Invalid file extension. The file to upload must have one of the following extension: jpg, jpeg, png, gif";
				} else {
					var self = this;		
					if(form.checkValidity()){ //save image
						makeCall("POST", 'UploadImage', form, function(req){
							if(req.readyState == 4){
								var res = req.responseText;
								if(req.status == 200){
									//simulate user interaction
									var e = document.createEvent("HTMLEvents");
									e.initEvent("change", false, true);
									document.getElementById("row").dispatchEvent(e);

								} else {
									alertmessage.textContent = res;
								}
							}
						});
					} else {
						form.reportValidity();
					}

				}
			});

			//show images of the album
			document.getElementById("row").addEventListener('change',function(){
				addImageForm.style.display = "none";
				addImageButtonDiv.style.display = "block";
				albumImages.show(albumImages.idalbum);
			});

			//submit comment form
			commentForm.elements["submit"].addEventListener('click', function(){
				var form = document.getElementById("id_addcommentform");
				if(form.elements["text"].value == ""){
					document.getElementById("id_addcommentalertmessage").textContent = "Cannot submit empty comment";
				} else {
					document.getElementById("id_addcommentalertmessage").innerHTML = "";
					var self = this;		
					if(form.checkValidity()){
						makeCall("POST", 'AddComment', form, function(req){
							if(req.readyState == 4){
								var res = req.responseText;
								if(req.status == 200){
									//simulate user interaction
									var e = document.createEvent("HTMLEvents");
									e.initEvent("change", false, true);
									commentForm.dispatchEvent(e);

								} else {
									document.getElementById("id_addcommentalertmessage").textContent = res;
								}
							}
						});
					} else {
						form.reportValidity();
					}
				}
			});

			//show modal image 
			commentForm.addEventListener('change',function(){
				imageDisplayed.show();
			});
			
			//hide form to add an image
			addImageForm.elements["cancel"].addEventListener('click', function(){
				addImageForm.reset();
				addImageForm.style.display = "none";
				addImageButtonDiv.style.display = "block";
			});
			
			//add listeners to all the thumbnails
			var d = document.getElementsByClassName("image");
			for(var i = 0; i < 5; ++i) {
				d[i].querySelector("img").addEventListener('mouseover',(e)=>{ 
					if(e.target.getAttribute("src") != ""){
						pageOrchestrator.displayImage(e.target.getAttribute("idimage"), 
								e.target.getAttribute("idalbum"), 
								e.target.getAttribute("src")); 
					}
				});
			}
			
			//add listener to next button
			document.getElementById("id_next").addEventListener('click', function(){
				albumImages.update(5);
			});
			
			//add listener to prev button
			document.getElementById("id_prev").addEventListener('click', function(){
				albumImages.update(-5);
			});
		}

		//open an album and show its images
		this.openAlbum = function(id, title){
			backToHomePageButton.style.visibility = "visible";
			albumsList.reset();
			albumImages.idalbum = id;
			albumImages.title = title;
			albumImages.currentPosition = 0;
			albumImages.show(id);
		};

		//open modal image
		this.displayImage = function(idimage, idalbum, src){
			imageDisplayed = new Image(idimage, idalbum);
			imageDisplayed.show();

		};

		//go back to the home page
		this.backToHomePage = function(){
			backToHomePageButton.style.visibility = "hidden";
			albumImages.reset();
			albumsList.refresh();
		};
	}

})();