(function() {

	var loginPage, registrationPage,
	errormsg = document.getElementById("errormessage"),
	errormsg2 = document.getElementById("errormessage2"),
	pageOrchestrator = new PageOrchestrator();

	//initialize page components
	window.addEventListener("load", () => {
		pageOrchestrator.start();
	}, false);

	function LoginPage(_loginform){
		this.loginForm = _loginform;

		//show login form
		this.show = function(){
			this.loginForm.style.display = "block";
		}

		//hide login form
		this.reset = function(){
			this.loginForm.style.display = "none";
		}

		//submit login form
		this.loginForm.elements["submit"].addEventListener('click', function(){
			var form = document.getElementById("loginform"); 

			if(form.elements["username"] == "" || form.elements["password"] == "" ){
				errormsg.textContent = "Please fill all fields of the form";
			} else {
				if(form.checkValidity()){
					makeCall("POST", 'CheckLogin', document.getElementById("loginform"), function(req){
						if(req.readyState == XMLHttpRequest.DONE){
							var message = req.responseText;
							if(req.status == 200){
								sessionStorage.setItem('username',message);
								window.location.href = "HomePage.html";
							} else {
								errormsg.textContent = message;
							}
						}
					});
				} else {
					form.reportValidity();
				}
			}
		});

		document.getElementById("registration").addEventListener('click', (e) => {
			pageOrchestrator.showRegistrationPage();
		});
	}


	function RegistrationPage(_registrationForm){
		this.registrationForm = _registrationForm;

		//show registration form
		this.show = function(){
			this.registrationForm.style.display = "block";
		}

		//hide registration form
		this.reset = function(){
			this.registrationForm.style.display = "none";
		}

		//submit registration form
		this.registrationForm.elements["submit"].addEventListener('click', (e) => {
			var form = document.getElementById("registrationform"),
			password = form.elements["password"].value,
			email = form.elements["email"].value,
			repeatPassword = form.elements["repeatPassword"].value,
			username = form.elements["username"].value,
			re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			if(username == "" || repeatPassword == "" || password == "" || email == ""){
				errormsg2.textContent = "Please fill all fields of the form";
			} else if(password != repeatPassword){
				errormsg2.textContent = "Password and repeat password do not match";
			} else if(!re.test(email)){
				errormsg2.textContent =  "Please input a valid email address";
			} else {
				if(form.checkValidity()){
					makeCall("POST", 'RegisterUser',form,function(req){
						if(req.readyState == XMLHttpRequest.DONE){
							var message = req.responseText;
							if(req.status == 200){
								sessionStorage.setItem('username', message);
								window.location.href = "HomePage.html";							
							} else {
								errormsg2.textContent = message;
							}
						}
					});
				} else {
					form.reportValidity();
				}
			}
		});

		document.getElementById("id_backtologin").addEventListener('click', (e) => {
			pageOrchestrator.showLoginPage();
		});
	}

	function PageOrchestrator(){

		//initialize components
		this.start = function(){
			loginPage = new LoginPage(document.getElementById("loginform"));
			registrationPage = new RegistrationPage(document.getElementById("registrationform"));
			this.showLoginPage();
		};

		//show login page
		this.showLoginPage = function(){
			registrationPage.reset();
			loginPage.show();
		};

		//show registration page
		this.showRegistrationPage = function(){
			loginPage.reset();
			registrationPage.show();
		}
	}

})();